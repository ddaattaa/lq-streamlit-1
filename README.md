<h1 align="center"><code> 📊: LQ-STREAMLIT-1 </code></h1>
<h2 align="center"><i> Testing out Streamlit </i></h2>

----
1. [Huh ?](#huh-)
2. [Installation Notes](#installation-notes)
   1. [Step 1 - Install `python-pip`](#step-1---install-python-pip)
   2. [Step 2 - Create Env](#step-2---create-env)
   3. [Step 3 - Test Streamlit](#step-3---test-streamlit)
 
----

# Huh ?

1. This is for learning [`https://docs.streamlit.io/`](https://docs.streamlit.io/) 
2. This is for making data apps 

# Installation Notes 


## Step 1 - Install `python-pip`

```sh 
python -m ensurepip --upgrade
pip install --upgrade pip
pip3 install pipenv
```
## Step 2 - Create Env

```sh
pipenv shell
pipenv install streamlit
```

## Step 3 - Test Streamlit

```sh 
streamlit hello
```