#!/bin/fish
 
echo "Update and Set Auto Install of dependencies "
echo "---- "
echo "Update "
echo "$> pnpm add -g pnpm"
echo "---"
pnpm add -g pnpm
pnpm --version
echp "UPDATE - Done!"
echo "DONE: pnpm update "
echo "$> pnpm config set auto-install-peers true"
echo "---"
pnpm config set auto-install-peers true
echo "DONE: Auto Install all Deps"
bat .npmrc